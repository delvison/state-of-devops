---
title:
- JCrew State of DevOps
---

# Current Issues
+ things are slow moving
+ messy communication (long email chains)
+ lack of notifications or unclear notifications
+ lack of a knowledgebase ([The Developer's Guide to JCrew](https://jcrewtracker.jira.com/wiki/spaces/~delvison.castillo/pages/284852528/The+Developer+s+Guide+to+JCrew))
+ broken legacy services (crewbot)

# DevOps Culture 
+ "DevOps is a culture, not a role! The whole company needs to be doing DevOps for it to work."

# FACT!!
+ Things are changing very fast and very frequently
+ We need to be more adaptable if we want to keep up!

## How?
+ Having a better defined culture 
+ Adapting our tooling
+ Minimizing "sources of truth"

# Infrastucture as Code

## Why?
+ Cloud + IAC = a perfect match

## Benefits
+ version control
+ improved visibility
+ modularity
+ swiftness

## How?
+ Terraform/Terragrunt 
  + written in HCL (declarative language)
  + state oriented

# IAC - Terraform 
+ [terraform-infrastructure.git](https://github.com/JCrew-Engineering/terraform-infrastructure)
+ [terraform-modules.git](https://github.com/JCrew-Engineering/terraform-modules)

# IAC - Terraform and Modules
![terraform modules](terraform-modules.png)

# Pipeline as Code

## Why?
+ improved manageability 
+ improved clarity
+ more control 

## How?
+ Jenkins Pipelines
+ Jenkinsfiles

# PAC - Jenkinsfiles
![jenkinsfile](pac.png)

# CI System
+ CI/Server which supports docker
+ Pipeline is oriented to containers.

![Pipeline](pipeline.png)

# CI System - Packaging Deliverables
+ Let's ditch RPM's
+ Dockerize everything we can
+ Build AMI's with packer
+ Immutable Infrastucture

# Testing
+ Which tools are we using?
+ API please.
+ Integration with Jenkins!!!  

# Defining a Stronger Culture
+ versioning 
+ better communication
+ clearly defined processes

# Versioning
SemVer v. RomVer
![](versioning.png)

# Better Communication - Slack
+ cutting back on noise
+ room per project?

# Developer Workflow - Builds
![ChatOps](slack-build.jpg)


# Thoughts? 
+ needs?
+ nice to haves?
