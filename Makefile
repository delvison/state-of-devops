build:
	pandoc presentation.md -t beamer -o pres.pdf

clean:
	rm pres.pdf

view:
	mupdf pres.pdf

ubuntu:
	sudo apt-get install texlive-latex-base texlive-latex-base texlive-latex-extra texlive-latex-recommended pandoc mupdf


